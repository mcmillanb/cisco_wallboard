import pandas as pd
#import urllib.request
import requests
import configparser
#import certifi
import os
import json
import time
import logging
import sys
import logging.handlers


# Import Config from ini file
config = configparser.ConfigParser()
config.optionxform = lambda option: option # Allow Capitals in config.ini when parsed

#### Get config values ###
def getconfig():
    global config
    config.read('wb.ini')

# Configure Syslog
configFilePath = "/app/ucce_collector.log"   
logger = logging.getLogger("ucce_collector")
logger.setLevel(logging.ERROR)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s level="%(levelname)s" process="python" logger="%(name)s" module="%(module)s" %(message)s', '%Y-%m-%d %H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler)
splunk_log = logging.getLogger("ucce_collector.splunk")

def callSLA(ServiceLevelCallsToday,CallsAnsweredToday):
    if CallsAnsweredToday == 0: return 100
    return round(100*(ServiceLevelCallsToday/CallsAnsweredToday),1) 

#### Pull report and log errors ###
def getReport(reportname,url):
    try:
        data = requests.get(url, verify=config['Permalinks'].getboolean('VerifySSL'))
        data.raise_for_status()
        splunk_log.info(reportname +" retrieved successfully")
        return data.text
    except requests.exceptions.RequestException as e:
        splunk_log.error(reportname +" retrieve failed: " + str(e))
        return None
    except requests.exceptions.HTTPError as e:
        splunk_log.error(reportname +" retrieve failed: " + str(e))
        return None

#### Retrieve and write Call Type Data ###
def getCTData():
    reportdata = getReport('CT Data',config['Permalinks']['wbcalltype_url'])
    if reportdata is not None:
        ctread = pd.read_html(reportdata)
        if config['Permalinks']['CallTypeKeyField'] in ctread[0].columns:
            ctTable = ctread[0]
        elif config['Permalinks']['CallTypeKeyField'] in ctread[1].columns:
            ctTable = ctread[1]
#        ctdata = ctTable.set_index(config['Permalinks']['CallTypeKeyField']).to_json(orient='index')
        ctTable['CallsAnsweredWithinSLA'] = 0
        for index, row in ctTable.iterrows():
            try:
                ctTable.at[index, 'CallsAnsweredWithinSLA'] = callSLA(int(ctTable.loc[index][config['Permalinks']['ServiceLevelCallsTodayField']]),int(ctTable.loc[index][config['Permalinks']['CallsAnsweredTodayField']]))
            except Exception as ex:
                print(ex)
                splunk_log.error("CT Data processing failed", ex)
        print(ctTable)
        write_json("ct.json",ctTable.set_index(config['Permalinks']['CallTypeKeyField']).to_json(orient='index'))

#### Retrieve and write Agent Data ###
def getAgentData():
    reportdata = getReport('Agent Data',config['Permalinks']['wbagentsgrt_url'])
    if reportdata is not None:
#        agentresponse = reportdata.read()
        agentread = pd.read_html(reportdata)
        print(reportdata)
        agentTable = agentread[0]
        agentTable = agentTable.dropna(subset=['Agent'])
        write_json("agent.json",agentTable.to_json(orient = 'index'))

#### Write Data to JSON ###
def write_json(fname,dataset):
        filename = '/app/static/data/'+fname
        with open(filename, 'w') as fp:
                fp.write(dataset)

while True:
    getconfig()
    getCTData()
    getAgentData()
    time.sleep(3)
