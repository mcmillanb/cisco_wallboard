from flask import Flask
from flask import render_template
from flask import send_from_directory
import configparser
import os
import json
import pandas as pd
import datetime
from pytz import timezone
import pytz
import logging
import sys
import logging.handlers

# Import Config from ini file
config = configparser.ConfigParser()
config.optionxform = str # Allow Capitals in config.ini when parsed

# Configure Syslog
configFilePath = "/app/ucce_wbdisplay.log"   
logger = logging.getLogger("ucce_wbdisplay")
logger.setLevel(logging.ERROR)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s level="%(levelname)s" process="python" logger="%(name)s" module="%(module)s" %(message)s', '%Y-%m-%d %H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler)
splunk_log = logging.getLogger("ucce_wbdisplay.splunk")

def getconfig():
    global config
    config.read('wb.ini')

def getData(dataname,filename):
    try:
        with open(filename) as ctdatafile:
            data = json.load(ctdatafile)
        splunk_log.info(dataname +" file loaded successfully")
        return data
    except:
        splunk_log.info(dataname +" file load error")
        return None



app = Flask(__name__)

def stat_type(csqueue,ctjson,box):
    boxconfig = config[csqueue.upper()][box].split(',')
    if boxconfig[0] == 'SLA':
        value = sla_percentage(csqueue,ctjson,boxconfig)
        return value
    elif boxconfig[0] == 'Num':
        value = thrnum(csqueue,ctjson,boxconfig)
        return value
    elif boxconfig[0] == 'MaxTime':
        value = maxTime(csqueue,ctjson,boxconfig)
        return value
    elif boxconfig[0] == 'AvTime':
        value = avTime(csqueue,ctjson,boxconfig)
        return value
    elif boxconfig[0] == 'ABASLA':
        value = abaSLA(csqueue,ctjson,boxconfig)
        return value
    elif boxconfig[0] == '':
        return ''

def time_to_seconds(timestr):
    if type(timestr) == float:
        sec_int = int(timestr)
    elif type(timestr) == int:
        sec_int = timestr
    elif type(timestr) == str:
        h,m,s = timestr.split(':')
        sec_int = int(h) * 3600 + int(m) * 60 + int(s)
    return sec_int

def seconds_to_str(seconds):
    sec_str = str('{:02}:{:02}:{:02}'.format(seconds//3600,seconds%3600//60, seconds%60))
    return sec_str


def avTime(csqueue,ctjson,boxconfig):
    times = []
    totalcalls = 0
    for queue in config[csqueue.upper()]['primaryStats'].split(','):
        totalcalls = totalcalls + ctjson[queue][config['Permalinks']['CallsAnsweredTodayField']]
        times.append(time_to_seconds(ctjson[queue]['AnswerWaitTimeToday']))
    try:
        av_time = round(sum(times)/totalcalls)
    except:
        av_time = 0
    thr_colour = ''
    op_time = str('{:02}:{:02}:{:02}'.format(av_time//3600,av_time%3600//60, av_time%60))
    op = '<td ' + thr_colour + '>'+ str(boxconfig[1].strip())+'<br><span>'+ str(op_time) + '</span></td>'
    return op
    
def maxTime(csqueue,ctjson,boxconfig):
    times = []
    for queue in config[csqueue.upper()]['primaryStats'].split(','):
        times.append(time_to_seconds(ctjson[queue][boxconfig[2]]))
    max_time = max(times)
    thr_colour = ''
    op_time = str('{:02}:{:02}:{:02}'.format(max_time//3600,max_time%3600//60, max_time%60))
    op = '<td ' + thr_colour + '>'+ str(boxconfig[1].strip())+'<br><span>'+ str(op_time) + '</span></td>'
    return op

def abaSLA(csqueue,ctjson,boxconfig):
    callsoffered = 0
    abandoned = 0
    for queue in config[csqueue.upper()]['primaryStats'].split(','):
        callsoffered = callsoffered + ctjson[queue][config['Permalinks']['CallsOfferedTodayField']]
        abandoned = abandoned + ctjson[queue][config['Permalinks']['CallsAbandonedTodayField']]
    if boxconfig[3] == 'N':
        op = '<td>'+ str(boxconfig[1].strip())+'<br><span>'+ str(abandoned) + '</span></td>'
        return op
    thr_colour = ''
    if callsoffered == 0:
        if abandoned >= 0:
            thr_colour = 'class="colorRed"'
        elif abandoned == 0:
            thr_colour = 'class="colorGreen"'
    elif callsoffered >= 0:
        abaToday = abandoned/callsoffered
        try:
            if abaToday > float(boxconfig[6].strip()):
                thr_colour = 'class="colorRed"'
            elif abaToday > float(boxconfig[5].strip()):
                thr_colour = 'class="colorOrange"'
            elif abaToday < float(boxconfig[5].strip()):
                thr_colour = 'class="colorGreen"'
        except:
            thr_colour = ''
    op = '<td ' + thr_colour + '>'+ str(boxconfig[1].strip())+'<br><span>'+ str(abandoned) + '</span></td>'
    return op


def slatotal(csqueue,ctjson):
    ServiceLevelCallsToday = 0
    CallsAnsweredToday = 0
    for queue in config[csqueue.upper()]['primaryStats'].split(','):
        ServiceLevelCallsToday = ServiceLevelCallsToday + ctjson[queue]['ServiceLevelCallsToday']
        CallsAnsweredToday = CallsAnsweredToday + ctjson[queue]['CallsAnsweredToday']
    if CallsAnsweredToday == 0:
        oneminsla =  100
    else:
        oneminsla = round(100*(ServiceLevelCallsToday/CallsAnsweredToday),2)
    return oneminsla

def sla_percentage(csqueue,ctjson,boxconfig):
        value = slatotal(csqueue,ctjson)
        if len(boxconfig) == 1:
            op = '<td></td>'
            return op
        if boxconfig[3] == 'N':
            op = '<td>'+ str(boxconfig[1].strip()) + '<br><span>' + str(value) +'%</span></td>'
            return op
        if value < int(boxconfig[6].strip()):
            thr_colour = 'class="colorRed"'
        elif value < int(boxconfig[5].strip()):
            thr_colour = 'class="colorOrange"'
        elif value > int(boxconfig[5].strip()):
            thr_colour = 'class="colorGreen"'
        op = '<td ' + thr_colour + '>'+ str(boxconfig[1].strip()) + '<br><span>' + str(value) +'%</span></td>'
        return op

def thrnum(csqueue,ctjson,boxconfig):
    value = 0
    try:
        if len(boxconfig) == 1:
            op = '<td></td>'
            return op
        elif len(boxconfig) >= 7:
            for queues in config[csqueue.upper()]['primaryStats'].split(','):
                value = value + ctjson[queues][boxconfig[2].strip()]
            thr_colour = ''
            if boxconfig[4].strip() == '>':
                if value > int(boxconfig[6].strip()):
                    thr_colour = 'class="colorRed"'
                elif value > int(boxconfig[5].strip()):
                    thr_colour = 'class="colorOrange"'
                elif value < int(boxconfig[5].strip()):
                    thr_colour = 'class="colorGreen"'
            elif boxconfig[4].strip() == '<':
                if value < int(boxconfig[6].strip()):
                    thr_colour = 'class="colorRed"'
                elif value < int(boxconfig[5].strip()):
                    thr_colour = 'class="colorOrange"'
                elif value > int(boxconfig[5].strip()):
                    thr_colour = 'class="colorGreen"'
            op = '<td ' + thr_colour + '>'+ str(boxconfig[1].strip())+'<br><span>'+ str(value) + '</span></td>'
            return op
        elif len(boxconfig) <= 7:
            for queue in config[csqueue.upper()]['primaryStats'].split(','):
                value = value + ctjson[queue][boxconfig[2].strip()]
            op = '<td>'+ str(boxconfig[1].strip())+'<br><span>'+ str(value)+'</span></td>'
            return op
    except: return ''

def clocktime(csqueue,clock):
    clockconfig = config[csqueue.upper()][clock].split(',')
    utc_now =pytz.utc.localize(datetime.datetime.utcnow())
    try:
        clockzone = clockconfig[1].strip()
        clocktime = utc_now.astimezone(timezone(clockzone))
        clockoutput = str(clockconfig[0])+'<br>'+ clocktime.strftime("%H:%M")
        return clockoutput
    except:
        clockoutput = ' '
        return clockoutput

def state_colour(state):
    if state == 'Not Ready':return 'class="state-green"'
    sortval = config['AgentState'][agentTable.loc[index]['Agent State']].split(',').strip()
    return ''

def agenttable(agentjson,csqueue):
    df = pd.DataFrame.from_dict(agentjson, orient='index')
    try:
        agenttabletitles = config[csqueue.upper()]['agentTableTitles'].split(',')
        op_table = '<tr><td class="title-style" style="width:5%;"></td><td class="title-style" style="width:45%;">'+ agenttabletitles[0] +'</td><td class="title-style" style="width:25%;">'+ agenttabletitles[1] +'</td><td class="title-style" style="width:30%;">'+ agenttabletitles[2] +'</td></tr>'
        tablesplit = config[csqueue.upper()]['TableSplit'].split(',')
    except: return 'Configuration Error'

    for team in tablesplit:
        agentteam = config[csqueue.upper()][team].split(',')
        team = df.loc[df[config['Permalinks']['AgentSkillField']] == agentteam[0]] 
        team_sorted = team.sort_values('Sort') 
        rows=27
        titlecolumntitle1 = ''
        for letters in agentteam[2]:
            tmp = letters + '<BR>'
            titlecolumntitle1 = titlecolumntitle1 + tmp
        titlecolumn1 = '<tr><td class="title-style" style="width:5%; background-color: #005569" text-align="center" rowspan="' + agentteam[1] + '">' + titlecolumntitle1
        op_table = op_table + titlecolumn1
        agenttable1rows = int(agentteam[1])
        for index, row in team_sorted.iterrows():
            if agenttable1rows ==0:
                break
            elif agenttable1rows >=0:
                sortval = config[csqueue][row['ReasonCodeValue']].split(',')
                dur_sec = time_to_seconds(row[config['Permalinks']['AgentDurationField']])
                dur_str = seconds_to_str(dur_sec)
                try:
                    if dur_sec >= (int(sortval[2]) * 60):
                        stcolour = 'class="blink-state" style="--pcolor:' + sortval[1] + ';"'
                    elif dur_sec <= (int(sortval[2]) * 60):
                        stcolour = 'class="state" style="--pcolor:' + sortval[1] + ';"'
                except:
                    stcolour = 'class="blink-state" style="--pcolor:' + sortval[1] + ';"'
                op_row = '<td>'+ str(row[config['Permalinks']['AgentNameField']]) + '</td>' + '<td><div ' + stcolour + '><span>'+ str(row['ReasonCodeValue']) + '</span></div></td>' + '<td>'+ dur_str + '</td></tr>'
                rows = rows-1
                agenttable1rows = agenttable1rows -1
                op_table = op_table + op_row
        while agenttable1rows >= 1:
            op_row = '</td><td></td><td></td><td></td></tr>'
            op_table = op_table + op_row
            rows=rows-1
            agenttable1rows = agenttable1rows -1
    return op_table

    
def middletable(ctjson,csqueue):
    try:
        middleTitles = config[csqueue.upper()]['middleTitles'].split(',')
        middleCTs = config[csqueue.upper()]['middleCTs'].split(',')
        middleStats = config[csqueue.upper()]['middleStats'].split(',')
        middletable = '<tr class="header">'
        tcnt = 0
        scnt = 0
        for title in middleTitles:
            if tcnt == 0:
                middletable = middletable + '<td class="qname">' + title + '</td>'
                tcnt = tcnt +1
            elif tcnt >= 0:
                middletable = middletable + '<td>' + title + '</td>'
        middletable = middletable + '</tr>'
        for queue in middleCTs:
            ctname = queue.split(':')
            middletable = middletable + '<tr><td class="qname">' + ctname[1] + '</td>'
            for stat in middleStats:
                middletable = middletable + '<td>' + str(ctjson[ctname[0]][stat]) + '</td>'
            middletable = middletable + '</tr>'
        return middletable
    except:
        return ''


@app.route('/')
def homepage():
    return render_template("nowb.html")

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
        'images/favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/<csqueue>')
def wallboard(csqueue):
    getconfig()
    if config.has_section(csqueue) == False:
        return render_template("n1.html",csqueue=csqueue)
    ctjson = getData('CT Data','/app/static/data/ct.json')
    agentjson = getData('CT Data','/app/static/data/agent.json')
    return render_template("t1.html", 
    csqueue=csqueue.upper(), 
    logo=config[csqueue]['logo'],
    background=config[csqueue]['background'],
    clock1=clocktime(csqueue,'clock1'),
    clock2=clocktime(csqueue,'clock2'),
    clock3=clocktime(csqueue,'clock3'),
    clock4=clocktime(csqueue,'clock4'),
    clock5=clocktime(csqueue,'clock5'),
    top1=stat_type(csqueue,ctjson,'top1'),
    top2=stat_type(csqueue,ctjson,'top2'),
    top3=stat_type(csqueue,ctjson,'top3'),
    bottom1=stat_type(csqueue,ctjson,'bottom1'),
    bottom2=stat_type(csqueue,ctjson,'bottom2'),
    bottom3=stat_type(csqueue,ctjson,'bottom3'),
    agenttable=agenttable(agentjson,csqueue),
    middletable=middletable(ctjson,csqueue)
    )


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)