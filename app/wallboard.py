import pandas as pd
import numpy as np
#import xml.etree.ElementTree as ET
import urllib.request
import requests
import ssl
import configparser
import os
import json
import time
import logging
import sys
import logging.handlers
#from bs4 import BeautifulSoup

ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)

# Import Config from ini file
config = configparser.ConfigParser()
config.optionxform = lambda option: option # Allow Capitals in config.ini when parsed

def getconfig():
    global config
    config.read('wb.ini')

# Configure Syslog
configFilePath = "/app/ucce_collector.log"   
logger = logging.getLogger("ucce_collector")
logger.setLevel(logging.ERROR)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s level="%(levelname)s" process="python" logger="%(name)s" module="%(module)s" %(message)s', '%Y-%m-%d %H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler)
splunk_log = logging.getLogger("ucce_collector.splunk")

def getReport(reportname,url):
    try:
        data = urllib.request.urlopen(url, context=ctx)
        splunk_log.info(reportname +" retrieved successfully")
        return data
    except urllib.error.URLError as e:
        splunk_log.error(reportname +" retrieve failed: " + str(e.reason))
        return None
    except urllib.error.HTTPError as e:
        splunk_log.error(reportname +" retrieve failed: " + str(e.code) + " : " + str(e.reason) + " : " + str(e.headers))
        return None

def getData():
    reportdata = getReport('CT Data',config['Permalinks']['wbcalltype_url'])
    if reportdata is not None:
        ctresponse = reportdata.read()
        ctread = pd.read_html(ctresponse)
        ctTable = ctread[1]
        ctTable['CallsAnsweredWithinSLA'] = 100
        for index, row in ctTable.iterrows():
            try:
                ctTable.at[index, 'CallsAnsweredWithinSLA'] = callSLA(int(ctTable.loc[index][config['Permalinks']['ServiceLevelCallsTodayField']]),int(ctTable.loc[index][config['Permalinks']['CallsAnsweredTodayField']]))
            except:
                splunk_log.error("CT Data processing failed")
        ctdata = ctTable.set_index(config['Permalinks']['CallTypeKeyField']).to_json(orient='index')
        print(ctTable)
        write_json("ct.json",ctTable.set_index(config['Permalinks']['CallTypeKeyField']).to_json(orient='index'))

    #### Retrieve and write Agent Data ###

    reportdata = getReport('Agent Data',config['Permalinks']['wbagentsgrt_url'])
    if reportdata is not None:
        agentresponse = reportdata.read()
        agentread = pd.read_html(agentresponse)
        agentTable = agentread[0]
        agentTable = agentTable.dropna(subset=['Agent'])
        agentTable['Sort'] = 0
        agentTable['ReasonCodeValue'] = ''
        for index, row in agentTable.iterrows():
            try:
                try:
                    if np.int64(row[config['Permalinks']['AgentReasonCodeField']]) == 999:
                        reason = row[config['Permalinks']['AgentStateField']]
                    else:
                        reason = config['ReasoncodeStates'][row[config['Permalinks']['AgentReasonCodeField']]]
                except:
                    reason = agentTable.loc[index][config['Permalinks']['AgentStateField']]
                agentTable.at[index,'ReasonCodeValue'] = reason
                sortval = config['AgentState'][reason].split(',')
                agentTable.at[index,'Sort'] = sortval[0]
            except:
                splunk_log.error("CT Data processing failed")
        write_json("agent.json",agentTable.to_json(orient = 'index'))

def callSLA(ServiceLevelCallsToday,CallsAnsweredToday):
    if CallsAnsweredToday == 0: return 100
    return round(100*(ServiceLevelCallsToday/CallsAnsweredToday),1) 
    
def write_json(fname,dataset):
        filename = '/app/static/data/'+fname
        with open(filename, 'w') as fp:
                fp.write(dataset)

while True:
    getconfig()
    getData()
    time.sleep(3)
