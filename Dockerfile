FROM alpine:3.8

#COPY Flask-1.0.2-py2.py3-none-any.whl Flask-1.0.2-py2.py3-none-any.whl
#COPY pandas-0.24.2.tar.gz pandas-0.24.2.tar.gz

LABEL description "Nginx + uWSGI + Flask based on Alpine Linux and managed by Supervisord"

# Copy python requirements file
COPY requirements.txt /tmp/requirements.txt

RUN apk update && \
    apk upgrade && \
    apk add --no-cache --virtual .build-deps python3-dev build-base && \
    apk add --no-cache python3 bash nginx uwsgi uwsgi-python3 openssl linux-headers pcre-dev py-lxml && \
    python3 -m ensurepip  && \
    rm -r /usr/lib/python*/ensurepip  && \
    pip3 install Flask requests numpy beautifulsoup4 html5lib circus  && \
    pip3 install pandas   && \
    apk del .build-deps  && \
    rm /etc/nginx/conf.d/default.conf
#    rm pandas-0.24.2.tar.gz && \
#    rm Flask-1.0.2-py2.py3-none-any.whl


# Copy the Nginx global conf
COPY ./nginx.conf /etc/nginx/
# Copy the Flask Nginx site conf
COPY ./flask-site-nginx.conf /etc/nginx/conf.d/
# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY ./uwsgi.ini /etc/uwsgi/
# Custom Supervisord config
#COPY supervisord.conf /etc/supervisord.conf
# Custom Circus config
COPY ./circus.ini /app/circus.ini

# Add demo app
#RUN mkdir /app
#COPY ./app /app
WORKDIR /app/

#CMD ["/usr/bin/supervisord"]
CMD ["/usr/bin/circusd", "circus.ini"]
